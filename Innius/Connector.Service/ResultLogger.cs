using System;
using System.Linq;
using Serilog;
using System.Reactive;
using Innius.Connector.Interfaces;

namespace Innius.Connector.Service
{
    internal static class ResultLogger
    {
        public static void LogResult(PutRecordsResponse resp)
        {
            Log.Information("processed {@count} records", resp.Records.Count - resp.FailedRecordCount);

            if (resp.FailedRecordCount <= 0) return;
            var errors = from err in resp.Records
                where err.ErrorMessage != string.Empty
                select err;

            Log.Error("errors while writing events to sink; {@Error}", errors.FirstOrDefault());
        }
    }
}