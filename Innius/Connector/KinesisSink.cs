using Innius.Connector.Interfaces;
using Amazon.Kinesis;
using Amazon.Kinesis.Model;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Reactive.Linq;
using System.Reactive;
using Newtonsoft.Json;
using Serilog;

using Microsoft.Extensions.Options;

namespace Innius.Connector
{
    public class KinesisSink : ISink
    {
        private IAmazonKinesis client;

        private string StreamName;

        public KinesisSink(IAmazonKinesis client, IOptions<SinkConfig> config) : this(client, config.Value) {}

        public KinesisSink(IAmazonKinesis client, SinkConfig options)
        {
            this.client = client;
            this.StreamName = options.StreamName;
        }
        public IObservable<Interfaces.PutRecordsResponse> PutRecords(IEnumerable<InniusEvent> events)
        {
            PutRecordsRequest request = new PutRecordsRequest()
            {
                Records = events.Select(x =>
                    {
                        string dataAsJson = JsonConvert.SerializeObject(x);
                        byte[] dataAsBytes = Encoding.UTF8.GetBytes(dataAsJson);
                        using (MemoryStream memoryStream = new MemoryStream(dataAsBytes))
                        {
                            PutRecordsRequestEntry requestRecord = new PutRecordsRequestEntry()
                            {
                                PartitionKey = x.MachineTrn,
                                Data = memoryStream,
                            };
                            return requestRecord;
                        }
                    }).ToList(),
                StreamName = this.StreamName,
            };
            if (request.Records.Count == 0)
            {
                return Observable.Empty<Interfaces.PutRecordsResponse>();
            }
            return Observable.FromAsync<Interfaces.PutRecordsResponse>(async () =>
            {
                var res = await client.PutRecordsAsync(request);

                return new Interfaces.PutRecordsResponse()
                {
                    FailedRecordCount = res.FailedRecordCount,
                    Records = (from r in res.Records select new Interfaces.PutRecordsResultEntry() { ErrorCode = r.ErrorCode, ErrorMessage = r.ErrorMessage }).ToList()
                };
            });
        }
    }

}