#Innius Connector

Innius .net core connector for opc-ua

This connector establishes a connection between an opc-ua server (source)  and innius sensor event kinesis stream (sink).

The connector is installed as a windows service. 

Open issues:
- innius.opcua.connector.Config.xml enables auto accept server certificates by default which is not allowed for production builds. 


## installation instructions
- obtain an innius token (you can get one from the innius dev team)
- open the postman console: 
    - create a new connection
        - write down the connection_id and token  
    - add a machine  
 
- download the zip from s3 to a customer windows machine  
- unzip the file to a folder
- start a powershell and go into this folder 
- type connector.windowsclient.exe action:install --token [TOKEN] --id [ID]
- start the innius.connector.service windows service 
- check the windows events log 
