using Innius.Connector.Interfaces;
using System.Reactive.Concurrency;
using System;
using System.Threading.Tasks;
using System.Reactive.Linq;
using System.Reactive;
using System.Reactive.PlatformServices;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System.Text;
using Serilog;
using Microsoft.Extensions.Options;
using Amazon.Runtime;
using static Amazon.Runtime.RefreshingAWSCredentials;
using Newtonsoft.Json;

namespace Innius.Connector
{
    public static class ObservableExtension
    {
        public static readonly Func<int, TimeSpan> ExponentialBackoff = n => TimeSpan.FromSeconds(Math.Pow(n, 2));

        public static IObservable<T> RetryWithBackoffStrategy<T>(
            this IObservable<T> source,
            int retryCount = 3,
            Func<int, TimeSpan> strategy = null,
            Func<Exception, bool> retryOnError = null,
            IScheduler scheduler = null)
        {
            strategy = strategy ?? ExponentialBackoff;
            scheduler = scheduler ?? Scheduler.Default;

            if (retryOnError == null)
                retryOnError = e => true;

            int attempt = 0;

            return Observable.Defer(() =>
                {
                    return ((++attempt == 1) ? source : source.DelaySubscription(strategy(attempt - 1), scheduler))
                        .Select(item => new Tuple<bool, T, Exception>(true, item, null))
                        .Catch<Tuple<bool, T, Exception>, Exception>(e => retryOnError(e)
                            ? Observable.Throw<Tuple<bool, T, Exception>>(e)
                            : Observable.Return(new Tuple<bool, T, Exception>(false, default(T), e)));
                })
                .Retry(retryCount)
                .SelectMany(t => t.Item1
                    ? Observable.Return(t.Item2)
                    : Observable.Throw<T>(t.Item3));
        }
    }

    internal class TokenRequest
    {
        [JsonProperty(PropertyName = "machineid")]
        public string machineid { get; set; }

        public TokenRequest(string trn)
        {
            machineid = trn;
        }
    }

    internal class TokenResponse
    {
        [JsonProperty(PropertyName = "AccessKeyId")]
        public string AccessKeyId { get; set; }

        [JsonProperty(PropertyName = "Expiration")]
        public string ExpirationDateString { get; set; } //ISO8601

        [JsonProperty(PropertyName = "SecretAccessKey")]
        public string SecretAccessKey { get; set; }

        [JsonProperty(PropertyName = "SessionToken")]
        public string SessionToken { get; set; }
    }

    public class NotAuthenticatedException : Exception
    {
        public NotAuthenticatedException(string message) : base(message)
        {
        }
    }

    public class RetryableException : Exception
    {
        public RetryableException(string message) : base(message)
        {
        }
    }

    public class NotModifiedException : Exception
    {
    }
   

    public class InniusBackend : IBackend, ICredentialsProvider
    {
        private readonly HttpClient _client;

        private readonly string ConnectionID;        

        public InniusBackend(HttpClient client, IOptions<BackendConfig> options)
        {
            this.ConnectionID = options.Value.Id;
            this._client = client;
        }

        private string ConfigurationEndpoint => $"{ConnectionID}/configuration";

        public IObservable<ServiceConfiguration> GetServiceConfiguration()
        {
            HttpRequestMessage Request() => new HttpRequestMessage(HttpMethod.Get, ConfigurationEndpoint);
            return GetServiceConfiguration(Request);
        }

        public IObservable<ServiceConfiguration> GetServiceConfiguration(EntityTagHeaderValue etag)
        {
            HttpRequestMessage Request() => new HttpRequestMessage(HttpMethod.Get, ConfigurationEndpoint)
            {
                Headers = {IfNoneMatch = {etag}}
            };

            return GetServiceConfiguration(Request);
        }

        private IObservable<ServiceConfiguration> GetServiceConfiguration(Func<HttpRequestMessage> request)
        {
            return Observable
                .FromAsync(() => SendAsync<ServiceConfiguration>(request(), (configuration, message) =>
                {
                    if (message.Headers.ETag != null) configuration.Etag = message.Headers.ETag;
                    return configuration;
                }))
                .Catch<ServiceConfiguration, NotModifiedException>(x => Observable.Empty<ServiceConfiguration>())
                .RetryWithBackoffStrategy(retryCount: 3, retryOnError: e => e is RetryableException);
        }

        private Task<T> SendAsync<T>(HttpRequestMessage request) => SendAsync<T>(request, null);

        private async Task<T> SendAsync<T>(HttpRequestMessage request, Func<T, HttpResponseMessage, T> callback)
        {
            var response = await _client.SendAsync(request);
            var body = await response.Content.ReadAsStringAsync();

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK:
                    if (callback == null) return JsonConvert.DeserializeObject<T>(body);
                    return callback(JsonConvert.DeserializeObject<T>(body), response);
                case HttpStatusCode.Unauthorized:
                    throw new NotAuthenticatedException(response.ReasonPhrase);
                case HttpStatusCode.BadGateway:
                    throw new RetryableException(response.ReasonPhrase);
                case HttpStatusCode.NotModified:
                    throw new NotModifiedException();
                default:
                    throw new Exception(
                        $"connection service returned an unexpected status {response.StatusCode}, path: {request.RequestUri}: {body}");
            }
        }

        public AWSCredentials AWSCredentials()
        {
            return new AWSCredentials(this);
        }

        public void  Heartbeat()
        {                   
            var request = new HttpRequestMessage(HttpMethod.Post, $"{ConnectionID}/heartbeat")
            {                
                Headers =
                {
                    {"x-connector-git-tag", ThisAssembly.Git.Tag},
                    {"x-connector-git-commit", ThisAssembly.Git.Commit},
                }
            };            
            SendAsync<TokenResponse>(request);
        }

        public IObservable<CredentialsRefreshState> TemporaryAWSCredentials()
        {
            return Observable
                .FromAsync<TokenResponse>(GetTemporaryAccessToken)
                .Select(tokenResponse =>
                {
                    var cred = new SessionAWSCredentials(tokenResponse.AccessKeyId, tokenResponse.SecretAccessKey,
                        tokenResponse.SessionToken);
                    var expiry = DateTime.Parse(tokenResponse.ExpirationDateString);
                    return new CredentialsRefreshState(cred.GetCredentials(), expiry);
                });
        }

        private Task<TokenResponse> GetTemporaryAccessToken()
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"{ConnectionID}/credentials");
            return SendAsync<TokenResponse>(request);
        }
    }
}