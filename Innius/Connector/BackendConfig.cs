namespace Innius.Connector.Interfaces
{
    public class BackendConfig
    {
        public string Id { get; set; }

        public string Token { get; set; }

        public string Endpoint { get; set; }
        
        public string Region { get; set; }
    }
}