using System.Collections.Generic;
using System;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Innius.Connector.Interfaces
{
    public interface IBackend
    {
        // Get the connection configuration
        IObservable<ServiceConfiguration> GetServiceConfiguration();

        // Get the connection configuration with an etag
        // This will return an empty observable if the service returned a 304
        IObservable<ServiceConfiguration> GetServiceConfiguration(EntityTagHeaderValue etag);

        AWSCredentials AWSCredentials();

        void Heartbeat();
    }
}