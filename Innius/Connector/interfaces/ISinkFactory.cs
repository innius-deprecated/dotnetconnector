namespace Innius.Connector.Interfaces
{
    public interface ISinkFactory
    {
        ISink Create(SinkConfig options);
    }
}