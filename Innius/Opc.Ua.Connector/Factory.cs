using System.Collections.Generic;
using Innius.Connector.Interfaces;
using Microsoft.Extensions.Options;

namespace Innius.Connector.Sources.Opc.Ua
{
    public class SourceFactory : ISourceFactory
    {
        public ISource Create(IEnumerable<Machine> machines, SourceConfig options)
        {
            return new OpcConnection(machines, options);
        }
    }
}