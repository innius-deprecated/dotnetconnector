using System;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Reactive;
using Innius.Connector.Interfaces;
using Serilog;
using Microsoft.Extensions.Options;
using System.Reactive.Subjects;
using System.Threading;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http.Headers;

namespace Innius.Connector
{
    // summary: ConfigurationProvider represents a reactive stream with connector configurations
    // the configurations are retrieved from the backend with an interval. 
    public class ConfigurationProvider : IConfigurationProvider
    {
        public TimeSpan Interval { get; set; } = TimeSpan.FromMinutes(1);

        private readonly IBackend _backend;

        private IObservable<ServiceConfiguration> _configStream;

        public ConfigurationProvider(IBackend backend)
        {
            _backend = backend;
        }

        private IObservable<ServiceConfiguration> GetServiceConfiguration()
        {
            EntityTagHeaderValue etag = null;
            var changes = Observable
                .Interval(Interval)
                .SelectMany((x) =>
                    etag == null ? _backend.GetServiceConfiguration() : _backend.GetServiceConfiguration(etag));

            var initial = _backend.GetServiceConfiguration();

            return initial
                .Merge(changes)
                .Distinct(config => config.Etag)
                // ReSharper disable once ImplicitlyCapturedClosure
                .Do(x => etag = x.Etag);
        }

        public IDisposable Subscribe(IObserver<ServiceConfiguration> observer)
        {
            if (_configStream == null) _configStream = GetServiceConfiguration();
            return _configStream.Subscribe(observer);
        }
    }
}