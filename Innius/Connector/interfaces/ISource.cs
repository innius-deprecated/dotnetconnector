using System.Reactive;
using System;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Innius.Connector.Interfaces
{
    public class InniusEvent
    {
        [JsonProperty(PropertyName = "machinetrn")]
        public string MachineTrn { get; set; }

        [JsonProperty(PropertyName = "sensorid")]
        public string SensorId { get; set; }

        [JsonProperty(PropertyName = "value")] public double Value { get; set; }

        [JsonProperty(PropertyName = "timestamp")]
        public long Timestamp;

        public string NodeId { get; set; }

        public InniusEvent(string machineId, string sensorId, double value, DateTimeOffset time)
        {
            MachineTrn = machineId;
            SensorId = sensorId;
            Value = value;
            SetTimestamp(time);
        }

        private void SetTimestamp(DateTimeOffset dateToConvert)
        {
            Timestamp = dateToConvert.ToUniversalTime().ToUnixTimeMilliseconds();
        }
    }

    public interface ISource : IObservable<InniusEvent>, IDisposable
    {
    }
}