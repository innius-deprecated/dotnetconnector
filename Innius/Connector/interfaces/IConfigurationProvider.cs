using System;

namespace Innius.Connector.Interfaces
{
    public interface IConfigurationProvider : IObservable<ServiceConfiguration>
    {
    }
}