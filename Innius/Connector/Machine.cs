using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using System.ComponentModel;

namespace Innius.Connector.Interfaces
{
    public class Machine
    {
        [JsonProperty(PropertyName = "id")] public string Id { get; set; }

        [JsonProperty(PropertyName = "sensors")]
        public IEnumerable<Sensor> sensors { get; set; }

        public Int64 Timestamp { get; set; }
    }

    public class Sensor
    {
        [JsonProperty(PropertyName = "id")] public string Id { get; set; }

        [JsonProperty(PropertyName = "node_id")]
        public string NodeId { get; set; }

        [JsonProperty(PropertyName = "rate")] public double Rate { get; set; }

        [DefaultValue(-1)]
        [JsonProperty(PropertyName = "position", DefaultValueHandling = DefaultValueHandling.Populate)]
        public int Position { get; set; }
    }
}