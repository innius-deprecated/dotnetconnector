﻿using System;
using Innius.Connector.Service;
using Mono.Options;
using Org.BouncyCastle.Asn1.Cms;
using PeterKottas.DotNetCore.WindowsService;
using PeterKottas.DotNetCore.WindowsService.Base;


namespace Innius.Connector.WindowsService
{
    class Program
    {
                
        public static void Main(string[] args)
        {                        
            ServiceRunner<ConnectorService>.Run(config =>
            {
                var name = config.GetDefaultName();
                Timer heartbeatTimer = null; 
                config.SetName("innius.connector.service");
                config.SetDescription("Connect innius to your local infrastructure");
                config.SetDisplayName("innius Connector Service");

                config.Service(serviceConfig =>
                {
                    
                    serviceConfig.ServiceFactory((extraArguments, controller) =>
                    {
                        // these variables will be set when the command line is parsed                        
                        var token = "";
                        var id = "";

                        // these are the available options, not that they set the variables
                        var options = new OptionSet
                        {
                            {"t|token=", "the token for the innius connection.", n => token = n},
                            {"i|id=", "the id of the innius connection.", s => id = s}
                        };
                        options.Parse(extraArguments);

                        Environment.SetEnvironmentVariable("innius_token", token,
                            EnvironmentVariableTarget.Machine);
                        Environment.SetEnvironmentVariable("innius_id", id, EnvironmentVariableTarget.Machine);

                        return new ConnectorService(controller);
                    });
                    
                    
                    serviceConfig.OnStart((service, extraParams) =>
                    {
                        Console.WriteLine("Service {0} started", name);
                        service.Start();

                        heartbeatTimer = new Timer("heartbeat", 1 * 60 * 1000 , service.HeartBeat);
                        heartbeatTimer.Start();
                    });

                    serviceConfig.OnStop(service =>
                    {
                        Console.WriteLine("Service {0} stopped", name);
                        service.Stop();
                        heartbeatTimer?.Stop();
                    });

                    serviceConfig.OnError(e =>
                    {
                        Console.WriteLine("Service {0} errored with exception : {1}", name, e.Message);
                    });
                });
            });
        }
    }
}