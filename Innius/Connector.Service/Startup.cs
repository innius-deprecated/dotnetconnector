using System;
using System.Net.Http;
using System.Net.Http.Headers;
using Amazon;
using Amazon.CloudWatchLogs;
using Amazon.Kinesis;
using Innius.Connector.Interfaces;
using Innius.Connector.Sources.Opc.Ua;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Serilog;
using IConfigurationProvider = Innius.Connector.Interfaces.IConfigurationProvider;

namespace Innius.Connector.Service
{
    public static class ServiceConfig
    {
        internal static IConfigurationRoot LoadConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(PlatformServices.Default.Application.ApplicationBasePath)
                .AddJsonFile("appsettings.json", optional: true)
                .AddEnvironmentVariables("innius_")
                .Build();
        }

        internal static ServiceProvider GetServiceProvider(IConfigurationRoot configuration)
        {
            var region = RegionEndpoint.GetBySystemName(configuration.Get<BackendConfig>().Region);
            
            var services = new ServiceCollection()
                .AddLogging(builder =>
                {
                    builder.SetMinimumLevel(LogLevel.Trace);
                    builder.AddSerilog();
                })
                .Configure<BackendConfig>(configuration.Bind)
                .AddOptions()
                .AddTransient<ISourceFactory, SourceFactory>()
                .AddTransient<ISinkFactory, SinkFactory>()
                .AddTransient<IConfigurationProvider, ConfigurationProvider>()
                .AddSingleton(configuration)
                .AddTransient(p =>
                {
                    var backend = p.GetRequiredService<IBackend>();
                    return backend.AWSCredentials();
                })
                .AddTransient<IAmazonCloudWatchLogs>(s =>
                {
                    var credentials = s.GetRequiredService<AWSCredentials>();
                    return new AmazonCloudWatchLogsClient(credentials, region);
                })
                .AddTransient<IAmazonKinesis>(s =>
                {     
                    var credentials = s.GetRequiredService<AWSCredentials>();                    
                    return new AmazonKinesisClient(credentials, region);
                })
                .AddTransient<IConnector, Connector>()
                .AddSingleton<IServiceLogger, ServiceLogger>();

            services.AddHttpClient<IBackend, InniusBackend>(
                HttpClientFactory(configuration.Get<BackendConfig>()));


            return services.BuildServiceProvider();
        }


        private static Action<HttpClient> HttpClientFactory(BackendConfig options)
        {
            return client =>
            {
                client.BaseAddress = new Uri(options.Endpoint);

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("x-connection-token", options.Token);

                client.Timeout = TimeSpan.FromMinutes(1);
            };
        }
    }
}