using System;
using Xunit;
using System.Threading.Tasks;
using System.Linq;
using Innius.Connector.Sources.Opc.Ua;
using Innius.Connector.Interfaces;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading;

namespace Innius.Connector.Sources.Opc.Ua
{
    public class OpcuaTest
    {
        [Fact]
        public async void TestOpcuaSource()
        {
            var options = new SourceConfig()
            {
                Endpoint = "opc.tcp://opcua.test.innius.com:51210/UA/SampleServer"
            };

            var machine = new Machine()
            {
                Id = "trn:foo:machine:bar",
                sensors = new List<Sensor>()
                {
                    new Sensor()
                    {
                        NodeId = "ns=4;i=1244",
                        Id = "s1",
                        Rate = 0.1
                    }
                }
            };

            var source = new OpcConnection(new List<Machine>() {machine}, options);

            var evt = await source.FirstOrDefaultAsync();

            Assert.Equal(machine.Id, evt.MachineTrn);
            Assert.Equal(machine.sensors.First().Id, evt.SensorId);
            Assert.NotEqual(0, evt.Value);
            Assert.NotEqual(0, evt.Timestamp);
        }

        [Fact]
        public void TestValidEvents()
        {
            Assert.True(OpcConnection.TryParse("3", out double result));
            Assert.Equal(3, result);
        }
    }
}