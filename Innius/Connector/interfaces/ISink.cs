using System.Collections.Generic;
using System.Threading.Tasks;
using System;

namespace Innius.Connector.Interfaces
{
    public class PutRecordsResponse
    {
        //
        // Summary:
        //     Gets and sets the property FailedRecordCount.
        //     The number of unsuccessfully processed records in a PutRecords request.
        public int FailedRecordCount { get; set; }

        //
        // Summary:
        //     Gets and sets the property Records.
        //     An array of successfully and unsuccessfully processed record results, correlated
        //     with the request by natural ordering. A record that is successfully added to
        //     a stream includes SequenceNumber and ShardId in the result. A record that fails
        //     to be added to a stream includes ErrorCode and ErrorMessage in the result.
        public List<PutRecordsResultEntry> Records { get; set; }
    }

    public class PutRecordsResultEntry
    {
        //
        // Summary:
        //     Gets and sets the property ErrorCode.
        //     The error code for an individual record result. ErrorCodes can be either ProvisionedThroughputExceededException
        //     or InternalFailure.
        public string ErrorCode { get; set; }

        //
        // Summary:
        //     Gets and sets the property ErrorMessage.
        //     The error message for an individual record result. An ErrorCode value of ProvisionedThroughputExceededException
        //     has an error message that includes the account ID, stream name, and shard ID.
        //     An ErrorCode value of InternalFailure has the error message "Internal Service
        //     Failure".
        public string ErrorMessage { get; set; }
    }

    public interface ISink
    {
        IObservable<PutRecordsResponse> PutRecords(IEnumerable<InniusEvent> events);
    }
}