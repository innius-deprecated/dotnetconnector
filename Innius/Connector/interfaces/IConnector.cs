using System;

namespace Innius.Connector.Interfaces
{
    public interface IConnector : IObservable<PutRecordsResponse>, IDisposable
    {
        ServiceConfiguration Configuration { get; set; }
    }
}