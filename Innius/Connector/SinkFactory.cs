using Amazon.Kinesis;
using Innius.Connector.Interfaces;

namespace Innius.Connector
{
    public class SinkFactory : ISinkFactory
    {
        private readonly IAmazonKinesis _kinesisClient;

        public SinkFactory(IAmazonKinesis client)
        {
            _kinesisClient = client;
        }

        public ISink Create(SinkConfig options)
        {
            return new KinesisSink(_kinesisClient, options);
        }
    }
}