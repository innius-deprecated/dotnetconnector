using System;
using System.Linq;
using Xunit;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Reactive.Linq;
using System.Reactive;
using Innius.Connector.Interfaces;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Reactive.Testing;
using Xunit.Sdk;

namespace Innius.Connector
{
    public class BackendTest
    {
        private const string ValidToken = "valid-token";

        private IBackend SetupTestBackend(string token)
        {
            const string backendUrl = "http://localhost:5555/";
            const string connectionId = "connection-id";

            var services = new ServiceCollection()
                .Configure<BackendConfig>(options =>
                {
                    options.Token = token;
                    options.Id = connectionId;
                });

            services.AddHttpClient<IBackend, InniusBackend>(client =>
            {
                client.BaseAddress = new Uri(backendUrl);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("x-connection-token", token);
            });

            var serviceProvider = services.BuildServiceProvider();

            return serviceProvider.GetRequiredService<IBackend>();
        }


        [Fact]
        public async void TestGetTemporaryAwsCredentials()
        {
            var backend = (InniusBackend) SetupTestBackend(ValidToken);
            var credentials = await backend.TemporaryAWSCredentials();

            Assert.Equal("AccessKeyId", credentials.Credentials.AccessKey);
            Assert.Equal("SecretKey", credentials.Credentials.SecretKey);
            Assert.Equal("Token", credentials.Credentials.Token);
        }

        [Fact]
        public async void TestGetTemporaryAwsCredentialsError()
        {
            var backend = (InniusBackend) SetupTestBackend("an invalid token");
            await Assert.ThrowsAsync<NotAuthenticatedException>(async () => await backend.TemporaryAWSCredentials());
        }


        [Fact]
        public async void GetConfiguration()
        {
            var backend = SetupTestBackend(ValidToken);

            var config = await backend.GetServiceConfiguration();
            var machines = config.Machines;
            Assert.NotEmpty(machines);
            Assert.Equal(2, machines.Count());
            var machine = machines.First();

            Assert.NotEmpty(machine.Id);
            Assert.NotEmpty(machine.sensors);
            var sensor = machine.sensors.First();

            Assert.NotEmpty(sensor.Id);
            Assert.NotEmpty(sensor.NodeId);
            Assert.NotEqual(0, sensor.Rate);

            var sensorStringNode = machine.sensors.ElementAt(1);
            Assert.NotEmpty(sensorStringNode.Id);
            Assert.Equal("ns=3,s=\"Innius sensors\".\"vacant1\"", sensorStringNode.NodeId);
            Console.WriteLine(sensorStringNode.NodeId);
            Assert.Contains('"', sensorStringNode.NodeId);
            Assert.DoesNotContain('\\', sensorStringNode.NodeId);
            
            Assert.Equal(new EntityTagHeaderValue("\"foo\""), config.Etag);
        }

        [Fact]
        public async void
            GetConfigurationWithEtag()
        {
            var backend = SetupTestBackend(ValidToken);
            var etag = new EntityTagHeaderValue("\"foo\"");

            Assert.True(await backend.GetServiceConfiguration(etag).IsEmpty());
        }

        [Fact]
        public async void RetryIfPendingDeployment()
        {
            var backend = SetupTestBackend("WithRetryableError");

            Assert.False(await backend.GetServiceConfiguration().IsEmpty());
        }

        [Fact]
        public async void WithConfigurationProvider()
        {
            var scheduler = new TestScheduler();

            var joop = scheduler.CreateObserver<ServiceConfiguration>();
            var backend = SetupTestBackend(ValidToken);

            var provider = new ConfigurationProvider(backend)
            {
                Interval = TimeSpan.FromSeconds(10)
            };
            //TODO: there must be a better way to test this            
            var count = 0;
            provider.Subscribe(x => count++);
            await Task.Delay(TimeSpan.FromMilliseconds(100));
            Assert.Equal(1, count);
        }
    }
}