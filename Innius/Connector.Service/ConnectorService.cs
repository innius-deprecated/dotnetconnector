using Microsoft.Extensions.DependencyInjection;
using PeterKottas.DotNetCore.WindowsService.Interfaces;
using System;
using System.Threading;
using Serilog;
using Innius.Connector.Interfaces;
using Microsoft.Extensions.Configuration;
using IConfigurationProvider = Innius.Connector.Interfaces.IConfigurationProvider;

namespace Innius.Connector.Service
{
    public class ConnectorService : IMicroService
    {
        private readonly ServiceProvider _serviceProvider;

        private IDisposable _configSubscriber;
        private IDisposable _connectorSubscriber;

        private IConnector _connector;
        private IConfigurationRoot _configuration;

        public ConnectorService(IMicroServiceController controller)
        {
            _configuration = ServiceConfig.LoadConfiguration();
            _serviceProvider = ServiceConfig.GetServiceProvider(_configuration);
        }

        public void HeartBeat()
        {
            _serviceProvider.GetRequiredService<IBackend>().Heartbeat();
        }

        public void Start()
        {            
            var serviceLogger = _serviceProvider.GetRequiredService<IServiceLogger>();
            Log.Logger = serviceLogger.DefaultLogger();
            var configurationProvider = _serviceProvider.GetRequiredService<IConfigurationProvider>();

            Log.Information("starting the service");

            StartConnector(configurationProvider, serviceLogger);
            
            Log.Information("service started");
        }


        private void Restart()
        {
            Stop();
            Start();
        }

        public void Stop()
        {
            Log.Information("stopping the service");
            _configSubscriber?.Dispose();
            
            Log.Information("service is stopped");
        }
        
        private void StartConnector(IConfigurationProvider configurationProvider1, IServiceLogger serviceLogger1)
        {
            _configSubscriber = configurationProvider1.Subscribe(
                (config) =>
                {
                    Log.Logger = serviceLogger1.CloudWatchLogger(config);
                    
                    Log.Information("received configuration {@config} from backend", config);
                    
                    _connector?.Dispose();
                    try
                    {
                        _connector = _serviceProvider.GetRequiredService<IConnector>();
                    }
                    catch (Exception e)
                    {
                        Log.Error(e, "could not start the connector");
                        throw;
                    }
                   
                    _connector.Configuration = config;

                    _connectorSubscriber?.Dispose();
                    _connectorSubscriber = _connector.Subscribe(
                        ResultLogger.LogResult, exception =>
                        {
                            Log.Error(exception,
                                "Connection error; restarting the connector in 10 seconds"); //TODO: exponential backoff?                    
                            Thread.Sleep(10000);
                            Restart();
                        });
                },
                (ex) =>
                {
                    Log.Error(ex,
                        "Connection error; restarting the connector in 10 seconds"); //TODO: exponential backoff?                    
                    Thread.Sleep(10000);
                    Restart();
                });
        }
    }
}