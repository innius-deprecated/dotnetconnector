using Amazon.CloudWatchLogs;
using Serilog.Sinks.AwsCloudWatch;
using Serilog.Sinks.EventLog;
using Serilog.Events;
using System;
using Serilog.Formatting;
using Serilog;
using Serilog.Formatting.Compact;
using System.Runtime.InteropServices;
using Innius.Connector.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Serilog.Core;


namespace Innius.Connector
{
    public interface IServiceLogger
    {
        ILogger DefaultLogger();
        ILogger CloudWatchLogger(ServiceConfiguration config);
    }

    public class ServiceLogger : IServiceLogger
    {
        private readonly IAmazonCloudWatchLogs _client;
        private readonly string _connectionId;

        private IConfigurationRoot _configurationRoot;


        public ServiceLogger(IAmazonCloudWatchLogs client, IConfigurationRoot configurationRoot,
            IOptions<BackendConfig> options)
        {
            _client = client;
            _connectionId = options.Value.Id;
            _configurationRoot = configurationRoot;
        }

        private LoggerConfiguration DefaultLoggerConfiguration(IConfigurationRoot configuration)
        {
            var logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .ReadFrom.Configuration(configuration)
                .Enrich.WithProperty("connection_id", _connectionId);

            WriteToEventLog(logger);

            return logger;
        }


        public ILogger DefaultLogger()
        {
            return DefaultLoggerConfiguration(_configurationRoot).CreateLogger();
        }

        public ILogger CloudWatchLogger(ServiceConfiguration options)
        {
            var cloudWatchSinkOptions = CloudWatchSinkOptions(options);

            return DefaultLoggerConfiguration(_configurationRoot)
                .WriteTo.AmazonCloudWatch(cloudWatchSinkOptions, _client)
                .CreateLogger();
        }

        private static CloudWatchSinkOptions CloudWatchSinkOptions(ServiceConfiguration options)
        {
            if (!Enum.TryParse(options.Logging.LogLevel, true, out LogEventLevel logLevel))
            {
                logLevel = LogEventLevel.Information;
            }

            ;

            var cloudWatchSinkOptions = new CloudWatchSinkOptions
            {
                // the name of the CloudWatch Log group for logging
                LogGroupName = options.Logging.LogGroupName,

                // the main formatter of the log event
                TextFormatter = new Serilog.Formatting.Compact.CompactJsonFormatter(),

                // other defaults defaults

                MinimumLogEventLevel = logLevel,

                BatchSizeLimit = 100,
                Period = TimeSpan.FromMinutes(1),
                CreateLogGroup = true,
                LogStreamNameProvider = new DefaultLogStreamProvider(),
                RetryAttempts = 0
            };
            return cloudWatchSinkOptions;
        }

        private static void WriteToEventLog(LoggerConfiguration logger)
        {
            if (!IsWindows()) return;
            logger.WriteTo.EventLog("innius-connector", manageEventSource: true,
                restrictedToMinimumLevel: LogEventLevel.Information);
        }


        private static bool IsWindows()
        {
            return System.Runtime.InteropServices.RuntimeInformation
                .IsOSPlatform(OSPlatform.Windows);
        }
    }
}