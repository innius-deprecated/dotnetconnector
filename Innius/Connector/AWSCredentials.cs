using Amazon.Runtime;
using System.Threading.Tasks;
using System.Reactive.Linq;
using System;
using System.Reactive.Threading.Tasks;
using Innius.Connector.Interfaces;

namespace Innius.Connector
{
    // AWS Credentials required for innius backend resources like kinesis and cloudwatch 
    public class AWSCredentials : RefreshingAWSCredentials
    {
        private ICredentialsProvider provider;

        public AWSCredentials(ICredentialsProvider provider)
        {
            this.provider = provider;
        }

        protected override Task<CredentialsRefreshState> GenerateNewCredentialsAsync()
        {                        
                return provider.TemporaryAWSCredentials().FirstAsync().ToTask();            
        }
    }
}