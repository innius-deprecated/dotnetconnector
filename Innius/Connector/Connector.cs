﻿using System;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Reactive;
using Innius.Connector.Interfaces;
using Serilog;
using Microsoft.Extensions.Options;
using System.Reactive.Subjects;
using System.Threading;
using System.Collections.Generic;
using System.Data;

namespace Innius.Connector
{
    public class Connector : IConnector, IDisposable, IObservable<PutRecordsResponse>
    {
        public ServiceConfiguration Configuration { get; set; }

        private readonly ISourceFactory _sourceFactory;
        private readonly ISinkFactory _sinkFactory;

        private ISource _source;
        private IObservable<PutRecordsResponse> _eventStream;

        public void Dispose()
        {
            _source?.Dispose();
        }

        public Connector(ISourceFactory sourceFactory, ISinkFactory sinkSinkFactory)
        {
            _sourceFactory = sourceFactory;
            _sinkFactory = sinkSinkFactory;
        }

        private IObservable<PutRecordsResponse> CreateEventStream(ServiceConfiguration options,
            IObservable<Machine> machines)
        {
            var sink = _sinkFactory.Create(options.Sink);

            _source = _sourceFactory.Create(options.Machines, options.Source);

            return _source
                .Buffer(TimeSpan.FromSeconds(10), 500) //TODO this should come from config 
                .Do(x => x.ToList().ForEach(value => Log.Debug("{@timestamp}: {@sensorid} => {@value}", value.Timestamp,
                    value.NodeId, value.Value)))
                .SelectMany(events => sink.PutRecords(events).Retry(3));
        }

        public IDisposable Subscribe(IObserver<PutRecordsResponse> observer)
        {
            if (_eventStream == null)
            {
                _eventStream = CreateEventStream(Configuration, Configuration.Machines.ToObservable());
            }

            return _eventStream.Subscribe(observer);
        }
    }
}