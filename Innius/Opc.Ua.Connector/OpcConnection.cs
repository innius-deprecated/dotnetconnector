using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using Innius.Connector.Interfaces;
using System.Reactive.Concurrency;
using Opc.Ua;
using Opc.Ua.Client;
using Opc.Ua.Configuration;
using Serilog;

namespace Innius.Connector.Sources.Opc.Ua
{
    public class OpcConnection : ISource
    {
        private static bool _autoAccept;
        private SessionReconnectHandler _reconnectHandler;
        private Lazy<Session> _session;

        private IObservable<InniusEvent> _events;

        private readonly List<MonitoredSensor> _sensors;
        private const int ReconnectPeriod = 10;

        public void Dispose()
        {
            if (_session.IsValueCreated) _session.Value.Dispose();
        }

        public OpcConnection(IEnumerable<Machine> machines, SourceConfig options)
        {
            _sensors = machines.SelectMany(machine =>
                machine.sensors.Select((sensor, idx) => new MonitoredSensor(machine, sensor, (uint) idx))).ToList();
            Log.Information("Sensors set {0}", _sensors.Count);
            _session = new Lazy<Session>(() => CreateSession(options.Endpoint).Result);
        }

        private class MonitoredSensor
        {
            public MonitoredItem MonitoredItem { get; }

            public Sensor Sensor { get; }

            public Machine Machine { get; }

            public MonitoredSensor(Machine machine, Sensor sensor, uint index)
            {
                Machine = machine;
                Sensor = sensor;
                MonitoredItem = new MonitoredItem(index)
                {
                    StartNodeId = new NodeId(sensor.NodeId),
                    IndexRange = (sensor.Position == -1 ? null : sensor.Position.ToString())
                };
            }
        }

        private static IObservable<InniusEvent> ValueStream(IEnumerable<MonitoredSensor> monitoredSensors)
        {
            var observable = monitoredSensors.ToObservable().SelectMany(sensor =>
            {
                return Observable
                    .FromEventPattern<MonitoredItemNotificationEventHandler, MonitoredItemNotificationEventArgs>(
                        x => Subscribe(x, sensor),
                        x => Unsubscribe(x, sensor))
                    .Select(args => args.EventArgs.NotificationValue as MonitoredItemNotification)
                    .Do(x => Log.Debug("received a value for nodeID {0} RequestHandle {1}: {2} - {3}",
                        sensor.Sensor.NodeId, x.ClientHandle, x.Message.PublishTime, x.Value.Value))
                    .Where(x => ValidEvent(x, sensor))
                    .Sample(TimeSpan.FromSeconds(sensor.Sensor.Rate))
                    .Select(value =>
                        new InniusEvent(sensor.Machine.Id, sensor.Sensor.Id, GetValue(value.Value),
                            value.Message.PublishTime) {NodeId = sensor.Sensor.NodeId});
            });
            return observable;
        }

        private static void Subscribe(MonitoredItemNotificationEventHandler x, MonitoredSensor sensor)
        {
            Log.Debug("Register event handler for {0}", sensor.MonitoredItem.StartNodeId);
            sensor.MonitoredItem.Notification += x;
        }

        private static void Unsubscribe(MonitoredItemNotificationEventHandler x, MonitoredSensor sensor)
        {
            Log.Debug("Unregister event handler for{0}", sensor.MonitoredItem.StartNodeId);
            sensor.MonitoredItem.Notification -= x;
        }

        // CreateSensorValueStream observable and subscribe to notification events 
        private IObservable<InniusEvent> CreateSensorValueStream()
        {
            var session = _session.Value;

            var subscription = new Subscription(session.DefaultSubscription)
                {PublishingInterval = 10}; //TODO publishing interval?                 
            subscription.Dispose();

            LogLastValues();

            var items = _sensors.Select(x => x.MonitoredItem).ToList();
            items.ForEach(item =>
                Log.Information("Adding item: {0}, position: {1}", item.StartNodeId, item.IndexRange));
            subscription.AddItems(items);

            session.AddSubscription(subscription);
            subscription.Create();

            return ValueStream(_sensors);
        }

        private void LogLastValues()
        {
            var session = _session.Value;

            session.ReadValues(_sensors.Select(x => new NodeId(x.Sensor.NodeId)).ToList(), new Type[_sensors.Count],
                out var values, out var errors);
            Log.Information("Retrieving last values for {0} sensors", _sensors.Count);
            for (int i = 0; i < _sensors.Count; i++)
            {
                Log.Information("{0}:{1}, {2} => {3}", _sensors[i].Sensor.Id, _sensors[i].Sensor.NodeId,
                    errors[i].StatusCode.ToString(), values[i] != null ? values[i].ToString() : "");
            }
        }

        public IDisposable Subscribe(IObserver<InniusEvent> observer)
        {
            if (_events == null) _events = CreateSensorValueStream();

            return _events.Subscribe(observer);
        }

        private static bool ValidEvent(MonitoredItemNotification value, MonitoredSensor sensor)
        {
            var valid = TryParse(value.Value, out var result);
            if (!valid)
            {
                Log.Warning("Incorrect sensorvalue: {0} {1} for {2}", value.Value, value.Value.StatusCode,
                    sensor.Sensor.NodeId);
            }

            return valid;
        }

        private static double GetValue(object value)
        {
            TryParse(value, out var result);
            return result;
        }

        public static bool TryParse(object value, out double result)
        {
            if (bool.TryParse(value.ToString(), out var boolResult))
            {
                result = boolResult ? 1 : 0;
                return true;
            }

            result = 0.0;
            if (!(value is bool)) return double.TryParse(value.ToString(), out result);
            result = (bool) value ? 1 : 0;
            return true;
        }

        private async Task<Session> CreateSession(string endpointUrl)
        {
            var application = new ApplicationInstance
            {
                ApplicationName = "innius OPC UA Connector",
                ApplicationType = ApplicationType.Client,
                ConfigSectionName = $"innius.opcua.connector{Environment.GetEnvironmentVariable("EnvironmentName")}"
            };

            var config = await application.LoadApplicationConfiguration(false);

            // check the application certificate.
            var haveAppCertificate = await application.CheckApplicationInstanceCertificate(false, 0);
            if (!haveAppCertificate)
            {
                throw new Exception("Application instance certificate invalid!");
            }

            config.ApplicationUri =
                Utils.GetApplicationUriFromCertificate(config.SecurityConfiguration.ApplicationCertificate.Certificate);
            if (config.SecurityConfiguration.AutoAcceptUntrustedCertificates)
            {
                _autoAccept = true;
            }
                       
            config.CertificateValidator.CertificateValidation += CertificateValidator_CertificateValidation;
            var selectedEndpoint = CoreClientUtils.SelectEndpoint(endpointUrl, true, 15000);
            var endpointConfiguration = EndpointConfiguration.Create(config);
            var endpoint = new ConfiguredEndpoint(null, selectedEndpoint, endpointConfiguration);

            var session = await Session.Create(config, endpoint, false, "innius-connector", 60000,
                new UserIdentity(new AnonymousIdentityToken()), null);
            
            // register keep alive handler
            session.KeepAlive += Client_KeepAlive;
            
            return session; 
        }

        private void Client_KeepAlive(Session sender, KeepAliveEventArgs e)
        {
            if (e.Status == null || !ServiceResult.IsNotGood(e.Status)) return;
            Log.Information("{0} {1}/{2}", e.Status, sender.OutstandingRequestCount, sender.DefunctRequestCount);

            if (_reconnectHandler != null) return;
            Log.Debug("--- Client_KeepAlive ---");
            _reconnectHandler = new SessionReconnectHandler();
            _reconnectHandler.BeginReconnect(sender, ReconnectPeriod * 1000, Client_ReconnectComplete);
        }

        private void Client_ReconnectComplete(object sender, EventArgs e)
        {
            // ignore callbacks from discarded objects.
            if (!ReferenceEquals(sender, _reconnectHandler))
            {
                return;
            }

            _session = new Lazy<Session>(() => _reconnectHandler.Session);
            _reconnectHandler.Dispose();
            _reconnectHandler = null;

            Log.Debug("--- Client_ReconnectComplete ---");
        }

        private static void CertificateValidator_CertificateValidation(CertificateValidator validator,
            CertificateValidationEventArgs e)
        {
            if (e.Error.StatusCode != StatusCodes.BadCertificateUntrusted) return;
            e.Accept = _autoAccept;
            if (_autoAccept)
            {
                Log.Information("Accepted Certificate: {0}", e.Certificate.Subject);
            }
            else
            {
                Log.Warning("Rejected Certificate: {0}", e.Certificate.Subject);
            }
        }
    }
}