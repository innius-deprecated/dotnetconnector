using System.Collections.Generic;

namespace Innius.Connector.Interfaces
{
    public interface ISourceFactory
    {
        ISource Create(IEnumerable<Machine> machines, SourceConfig options);
    }
}