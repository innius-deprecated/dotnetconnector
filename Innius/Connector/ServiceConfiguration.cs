using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using Microsoft.Extensions.Configuration.Json;
using Microsoft.Extensions.Configuration;
using Innius.Connector.Interfaces;
using Newtonsoft.Json;

namespace Innius.Connector.Interfaces
{
    public class SinkConfig
    {
        public string StreamName { get; set; }
    }

    public class SourceConfig
    {
        public string Endpoint { get; set; }
    }

    public class LoggingOptions
    {
        [JsonProperty(PropertyName = "log_level")]
        public string LogLevel { get; set; }

        [JsonProperty(PropertyName = "log_group_name")]
        public string LogGroupName { get; set; }
    }

    public class ServiceConfiguration
    {
        [JsonProperty(PropertyName = "timestamp")]
        public Int64 Timestamp { get; set; }

        [JsonProperty(PropertyName = "machines")]
        public List<Machine> Machines { get; set; }

        [JsonProperty(PropertyName = "source")]
        public SourceConfig Source { get; set; }

        [JsonProperty(PropertyName = "sink")] public SinkConfig Sink { get; set; }

        public EntityTagHeaderValue Etag { get; set; }

        [JsonProperty(PropertyName = "logging")]
        public LoggingOptions Logging { get; set; }
    }
}