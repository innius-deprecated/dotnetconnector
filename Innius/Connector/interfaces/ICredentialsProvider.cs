using Amazon.Runtime;
using System.Threading.Tasks;
using static Amazon.Runtime.RefreshingAWSCredentials;
using System.Reactive.Linq;
using System;

namespace Innius.Connector.Interfaces
{
    public interface ICredentialsProvider
    {
        IObservable<CredentialsRefreshState> TemporaryAWSCredentials();
    }
}